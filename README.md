
# GitLab Workflow

[2019년 9월 25일 GitLab Korea Meetup #5](https://festa.io/events/520)에서 발표된 슬라이드 입니다.

## GitLab Page 에서 보기
브라우져로 다음 페이지를 열면 슬라이드가 열립니다.
- https://gitlab-kr.gitlab.io/MeetUp/201909-5th/gitlab-workflow-slides

## Docker 실행해서 보기
```
$ docker run --name 201909-devops-kr-slide -p 9090:80 registry.gitlab.com/gitlab-kr/MeetUp/201909-5th/gitlab-workflow-slides
```
브라우져를 통해 **http://localhost:9090** 에서 확인 할 수 있습니다.


## 체크아웃 받아서 보기
node 8.x 이상이 필요 합니다.

```
$ git clone https://gitlab.com/GitLab-KR/MeetUp/201909-5th/gitlab-workflow-slides.git
$ cd msa-gitlab-slide
$ npm install
$ npm start
```
브라우져를 통해 **http://localhost:9090** 에서 확인 할 수 있습니다.
