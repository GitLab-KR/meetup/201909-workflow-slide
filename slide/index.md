---
marp: false
title: GitLab Workflow
description: 
// theme: uncover
theme: clean
paginate: true
page_number:false
footer: 
---

<style scoped>li {font-size:80%;} </style>

# <!--fit-->  GitLab Workflow

> GitLab Workflow로 Agility를 높이는 (...) 방법!

<style scoped>h6 { text-shadow: 0 0 20px #fff; text-align: center; }</style>

###### 신철호

---

<!--
_backgroundColor: #123
_color: #fff
-->


# 👉 발표내용

 > GitLab Workflow Review
 > Demo

---

# Bespoke :smirk:

> 비스포크 = 맞춤제작, 맞춤주문 (맞춤복, 정장, 패션쪽 용어)

---

# 발표자 소개
### 주 4 일 - 돈벌기
* 소프트웨어 개발~품질~운영 전 라이프사이클 관리 및 솔루션 컨설팅
* ALM, CI/CD, DevOps 구축 및 운영 관리
* 마지막 '소프트웨어 개발` 프로젝트?
  * 2017.03 - CI/CD 자동화 도구개발 - Gerrit+Jenkins+Docker

### 주 2 일 - 돈쓰기
* `감사한분께 선물하기` 프로젝트 진행 
  * 100개 후추통 = 감사한분께 선물하기
  * `2020년 오픈예정`

---

<!--
_backgroundColor: #123
_color: #fff
-->
# <!--fit-->  :relaxed: GitLab Workflow :heart_eyes:   

---

<!--
_backgroundColor: #123
_color: #fff
-->

# 발표 목표
## <!-- fit --> GitLab Workflow 로 Agility를 높인다.

---

# <!-- fit --> Agility?

---

# Agile S/W Development?
# <!-- fit --> 애자일 개발은 리스크를 줄이기 위한 방법

---

# 제 1 원칙
## 가장 높은 우선순위는 가치있는 소프트웨어를 일찍 그리고 지속적으로 전달해서 고객을 만족시키는 것이다.
> Our highest priority is to satisfy the customer through early and continuous delivery of valuable software.

---
<!--
_backgroundColor: #000
_color: #fff
-->

# <!-- fit --> Done = Deployed

---

# <!-- fit --> Workflow?
>  보편적인 솔루션 없음
> 도구, 프로세스 그리고 사람
> 유연함이 핵심!

---

# <!-- fit --> GitLab Workflow

![gitlab workflow](images/gitlab-workflow-1.png)

---

# GitLab Workflow - 개별 워크플로우 

![gitlab workflow](images/gitlab-workflow-2.png)

---

# GitLab Workflow - 세부 메소드들을 살펴보면

![gitlab workflow](images/gitlab-workflow-3.png)

---

#  프로젝트 협업 워크플로우
![right bg 80% gitlab workflow code](images/gitlab-workflow-issue-overview.png)

* Backlog Grooming -> `Epic, Issue, Label`
* Sprint Planning -> `Milestone`
* Burndown -> `Issue Board` 
  (feat Kanban)
* Velocity -> `Cycle Time`

---
# 프로젝트 협업 워크플로우
![gitlab workflow issue](images/gitlab-workflow-issue.png)

---

# 코드 협업 워크플로우 리뷰
![right bg 80% gitlab workflow code](images/gitlab-workflow-code-overview.png)

* Git 브랜칭
  * Git Flow
  * Git Common Flow (GitHub)
  * GitLab Flow
* 코드 리뷰 및 승인

---

# Git Flow
![git flow height:550](images/git-flow.png)

---
# Git Common Flow
![git flow height:300](images/git-common-flow-1.0.0-rc.5.svg)

---

# GitLab Flow - Production 
![git flow height:500](images/gitlab-workflow-production_branch.png)

---

# GitLab Flow - Environment
![git flow height:500](images/gitlab-workflow-environment_branches.png)

---

# GitLab Flow - Stable Release
![git flow height:500](images/gitlab-workflow-release_branches.png);

---

# CI/CD 워크플로우
![gitlab workflow cicd](images/gitlab-workflow-cicd-overview.png)


---

# CI/CD 워크플로우
![gitlab workflow cicd](images/gitlab-workflow-cicd.png)

---

<!--
_backgroundColor: #000
_color: #fff
-->
# Demo
## <!-- fit --> MeetUp 슬라이드 준비, GitLab Workflow 따라하기

---

# <!-- fit --> Q&A

---

## Referneces 
- [GitLab Workflow](https://docs.gitlab.com/ee/workflow/)
- [GitLab Flow](https://docs.gitlab.com/ee/workflow/gitlab_flow.html)
